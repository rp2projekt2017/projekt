<?php 

class Poruka {

	public $id_poruke, $id_sender, $id_receiver, $sadržaj, $vrijeme_slanja; 

	function __construct($id_poruke, $id_sender, $id_receiver, $sadržaj, $vrijeme_slanja){
		$this->id_poruke = $id_poruke;
		$this->id_sender = $id_sender;
		$this->id_receiver = $id_receiver;
		$this->sadržaj = $sadržaj;
		$this->vrijeme_slanja = $vrijeme_slanja;
	}

	function __get($prop){
		return $this->$prop;
	}
	function __set($prop, $val){
		$this->$prop = $val; return $this;
	}
}

