<?php


class SiteService {


	public function getPopisById($id){

		try {
			$db = DB::getConnection();
			$sent = $db->prepare("SELECT id_receiver, COUNT(*) broj_poruka, MAX(vrijeme_slanja) zadnja_poruka FROM poruka GROUP BY id_sender, id_receiver HAVING id_sender = ?");
			$sent->execute([$id]);

			$received = $db->prepare("SELECT id_sender, COUNT(*) broj_poruka, MAX(vrijeme_slanja) zadnja_poruka FROM poruka GROUP BY id_sender, id_receiver HAVING id_receiver = ?");
			$received->execute([$id]);

		} catch(PDOException $e){
			exit('PDO error ' . $e->getMessage());
		}

		$stats = [];
		while($row = $sent->fetch()){
			$temp_nick = $this->getKorisnikNickByID($row['id_receiver']);
			$stats[$row['id_receiver']] = ['id' => $row['id_receiver'], 'kontakt' => $temp_nick, 'count' => (int) $row['broj_poruka'], 'last' => $row['zadnja_poruka']];
		}

		while($row = $received->fetch()){
			if(!isset($stats[$row['id_sender']])){
				$temp_nick = $this->getKorisnikNickByID($row['id_sender']);
				$stats[$row['id_sender']] = ['id' => $row['id_sender'], 'kontakt' => $temp_nick, 'count' => 0, 'last' => $row['zadnja_poruka']];
			}
			$stats[$row['id_sender']]['count'] += (int) $row['broj_poruka'];
			$stats[$row['id_sender']]['last'] = max([$stats[$row['id_sender']]['last'], $row['zadnja_poruka']]);
		}

		return $stats;

	}

	public function getLocation($id){
		try {
			$db = DB::getConnection();
			$dohvati = $db->prepare('SELECT lng, lat FROM korisnik WHERE id_korisnika = ?');
			$dohvati->execute([$id]);

		} catch(PDOException $e){
		  exit('PDO error ' . $e->getMessage());
		}
		$arr = [];
		$dio = $dohvati->fetch();
		$arr['lng'] = $dio['lng'];
		$arr['lat'] = $dio['lat'];
		return $arr;
		}

		public function Udaljenost($nick1, $nick2) {
			$id1 = $this->getKorisnikIdByNick($nick1); $id2 = $this->getKorisnikIdByNick($nick2);
			$prvi = $this->getLocation($id1); $drugi = $this->getLocation($id2);
			$lat1 = $prvi['lat']; $lon1 = $prvi['lng'];
			$lat2 = $drugi['lat']; $lon2 = $drugi['lng'];

			if($lat1 == NULL || $lon1 == NULL || $lat2 == NULL || $lon2 == NULL) return -1;

  		$theta = $lon1 - $lon2;
  		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  		$dist = acos($dist);
  		$dist = rad2deg($dist);
  		$miles = $dist * 60 * 1.1515;

      return ($miles * 1.609344);

}



	public function setLocation($id){
		$lat = floatval($_POST['lat']); $lng = floatval($_POST['lng']);
		try {
			$db = DB::getConnection();
			$ubaci = $db->prepare('UPDATE korisnik SET lat = ?, lng = ? WHERE id_korisnika = ?');
			$ubaci->execute([$lat, $lng, $id]);

		} catch(PDOException $e){
		  exit('PDO error ' . $e->getMessage());
		  echo FALSE;
		}
	}


	public function postoPodudaranja($id_prva, $id_druga){//FUNKCIJA VRACA U KOJEM POSTOTKU SE PODUDARAJU KORISNICI
		$podudaranja = 0; $ukupno = 0; $udalj;
		try {
			$db = DB::getConnection();
			$trazim = $db->prepare('SELECT id_interesa FROM korisnik_interes WHERE id_korisnika LIKE :id AND vrsta_relacije LIKE 2');
			$trazim->execute(array('id' => $id_prva));
			$drugi_ima = $db->prepare('SELECT id_interesa FROM korisnik_interes WHERE id_korisnika LIKE :id AND vrsta_relacije LIKE 1');
			$drugi_ima->execute(array('id' => $id_druga));

		} catch(PDOException $e){
			exit('PDO error ' . $e->getMessage());
		}

		while ($tr = $trazim->fetch()){
			$ukupno++;
			while($ima = $drugi_ima->fetch()){
				if ($tr['id_interesa'] === $ima['id_interesa']) $podudaranja++;
			}
		}

		return $podudaranja/$ukupno;
	}

	public function nadiNajboljeg($id){//VRACA ID KORISNIKA KOJI JE NAJKOMATIBILNIJI, NE RADI JER NE NALAZI FUNKCIJU POSTO PODUDARANJA
		$max = 0; $id_max = -1; $br = 0;
		try {
			$db = DB::getConnection();
			$id_ostatka = $db->prepare('SELECT id_korisnika, gender, interested_in FROM korisnik');
			$id_zanima = $db->prepare('SELECT gender, interested_in FROM korisnik WHERE id_korisnika LIKE :id');
			$id_ostatka->execute();
			$id_zanima->execute(array("id"=>$id));

		} catch(PDOException $e){
		 exit('PDO error ' . $e->getMessage());
	 }
	 while ($red = $id_zanima->fetch()){ $zanima_me =  $red['interested_in']; $moj_spol = $red['gender']; }

	 while ($red = $id_ostatka->fetch()){
		 if ($red['id_korisnika'] != $id && $red['interested_in']===$moj_spol && $red['gender']===$zanima_me ||
	 			 $red['id_korisnika'] != $id && $red['interested_in']===$moj_spol && $zanima_me==="o" ||
				 $red['id_korisnika'] != $id && $red['interested_in']==="o" && $red['gender']===$zanima_me)
				 		{ $br = $this->postoPodudaranja($id, $red['id_korisnika']); }
		 if ($br > $max) { $id_max = intval($red['id_korisnika']); $max = $br; }
	 }

	 return $id_max;
	}

	public function getImeOpisDatum(){
		try {
			$db = DB::getConnection();
			$st = $db->prepare('SELECT nick, ime, dat_rođenja, path_slike, about FROM korisnik');
			$st->execute();
		} catch(PDOException $e){
			exit('PDO error ' . $e->getMessage());
		}
		$arr = [];
		while($row = $st->fetch()){
			if ($row['ime'] !== "test"){
				$red = [];
				$red['nick'] = $row['nick'];
				$red['ime'] = $row['ime'];
				$red['dat_rođenja'] =  $row['dat_rođenja'];
				$red['path_slike'] = $row['path_slike'];
				$red['about'] = $row['about'];
				if (count($arr) < 4) array_push($arr, $red);
			}
		}

		return $arr;
}


	public function getSviInteresi(){
		try {
			$db = DB::getConnection();
			$st = $db->prepare('SELECT id_interesa, ime_interesa FROM interes');
			$st->execute();
		} catch(PDOException $e){
			exit('PDO error ' . $e->getMessage());
		}

		$arr = [];
		while($row = $st->fetch()){
			$arr[] = new Interes($row['id_interesa'], $row['ime_interesa']);
		}

		return $arr;
	}

	public function getPodacibyNick($nick){
		try {
			$db = DB::getConnection();
			$st = $db->prepare('SELECT id_korisnika, ime, prezime, dat_rođenja, gender, path_slike, mail, interested_in, about FROM korisnik WHERE nick LIKE :nick');
			$st->execute(array("nick"=>$nick));
		} catch(PDOException $e){
			exit('PDO error ' . $e->getMessage());
		}

		$row = $st->fetch();
		return $row;
	}

	public function getInteresibyIDkorisnika($id, $vrsta){
		try {
			$db = DB::getConnection();
			$st = $db->prepare('SELECT id_interesa FROM korisnik_interes WHERE id_korisnika LIKE :id AND vrsta_relacije LIKE :vrsta');
			$st->execute(array("id"=>$id, "vrsta"=>$vrsta));
		} catch(PDOException $e){
			exit('PDO error ' . $e->getMessage());
		}
		$int = $this->getSviInteresi();
		$arr = [];

		while ($row = $st->fetch()){
			foreach($int as $ime) if($ime->id_interesa === $row['id_interesa'])
			array_push($arr, $ime->ime_interesa);
		}

		return $arr;
	}

	

	public function brojPoruka($id1, $id2){
		try {
			$db = DB::getConnection();
			$st1 = $db->prepare("SELECT id_poruke FROM poruka WHERE id_sender = ? AND id_receiver = ?");
			$st1->execute([$id1, $id2]);
			$st2 = $db->prepare("SELECT id_poruke FROM poruka WHERE id_sender = ? AND id_receiver = ?");
			$st2->execute([$id2, $id1]);
		} catch(PDOException $e){
			exit('PDO error ' . $e->getMessage());
		}
		$br = 0;
		while ($row = $st1->fetch()){ $br++; }
		while ($row = $st2->fetch()){ $br++; }
		return $br;
	}

	public function posaljiPoruku($id1, $id2, $poruka){
		try {
			$db = DB::getConnection();
			$st = $db->prepare("INSERT INTO poruka (id_sender, id_receiver, sadržaj) VALUES (?, ?, ?)");
			$st2 = $db->prepare("UPDATE korisnik SET poruka_flag = 1 WHERE id_korisnika = ?");
			$st->execute([$id1, $id2, $poruka]);
			$st2->execute([$id2]);
		} catch(PDOException $e){
			exit('PDO error ' . $e->getMessage());
		}

		return TRUE;
	}

	public function provjeriPoruku($id){
		try {
			$db = DB::getConnection();
			$st = $db->prepare("SELECT poruka_flag FROM korisnik WHERE id_korisnika = ?");
			$st->execute([$id]);
		} catch(PDOException $e){
			exit('PDO error ' . $e->getMessage());
		}
		$row = $st->fetch();
		return $row['poruka_flag'];
		
	}
	public function ubijPoruku($id){
		try {
			$db = DB::getConnection();
			$st = $db->prepare("UPDATE korisnik SET poruka_flag = 0 WHERE id_korisnika = ?");
			$st->execute([$id]);
		} catch(PDOException $e){
			exit('PDO error ' . $e->getMessage());
		}
		
	}



	public function getMailByID($id){
		try {
			$db = DB::getConnection();
			$st = $db->prepare("SELECT mail FROM korisnik WHERE id_korisnika = ?");
			$st->execute([$id]);
		} catch(PDOException $e){
			exit('PDO error ' . $e->getMessage());
		}
		$row = $st->fetch();
		return $row['mail'];
	}

	public function getImeByID($id){
		try {
			$db = DB::getConnection();
			$st = $db->prepare("SELECT ime FROM korisnik WHERE id_korisnika = ?");
			$st->execute([$id]);
		} catch(PDOException $e){
			exit('PDO error ' . $e->getMessage());
		}
		$row = $st->fetch();
		return $row['ime'];
	}

	public function getPrezimeByID($id){
		try {
			$db = DB::getConnection();
			$st = $db->prepare("SELECT prezime FROM korisnik WHERE id_korisnika = ?");
			$st->execute([$id]);
		} catch(PDOException $e){
			exit('PDO error ' . $e->getMessage());
		}
		$row = $st->fetch();
		return $row['prezime'];
	}

	public function getImgPath($id){
		try {
			$db = DB::getConnection();
			$st = $db->prepare("SELECT path_slike  FROM korisnik WHERE id_korisnika = ?");
			$st->execute([$id]);
		} catch(PDOException $e){
			exit('PDO error ' . $e->getMessage());
		}
		$row = $st->fetch();
		return $row['path_slike'];
}

	public function setImgPath($path, $id){
		try {
			$db = DB::getConnection();
			$st = $db->prepare("UPDATE korisnik  SET path_slike = ? WHERE id_korisnika = ?");
			$st->execute([$path, $id]);
		} catch(PDOException $e){
			exit('PDO error ' . $e->getMessage());
		}
		return TRUE;
}

	public function getPorukeByReceiverAndSenderID($ids, $idr){
		try {
			$db = DB::getConnection();
			$st = $db->prepare("SELECT id_poruke, id_sender, id_receiver, sadržaj, vrijeme_slanja FROM poruka WHERE (id_sender = $ids AND id_receiver = $idr) OR (id_sender = $idr AND id_receiver = $ids) ORDER BY vrijeme_slanja ASC");
			$st->execute();
		} catch(PDOException $e){
			exit('PDO error ' . $e->getMessage());
		}

		$arr = [];
		while($row = $st->fetch()){
			$arr[] = new Poruka($row['id_poruke'], $row['id_sender'], $row['id_receiver'], $row['sadržaj'], $row['vrijeme_slanja']);
		}

		return $arr;
	}


	public function getKorisnikByNick($nick){

		try {
			$db = DB::getConnection();
			$st = $db->prepare("SELECT id_korisnika, nick, pass FROM korisnik WHERE nick = ?");
			$st->execute([$nick]);
		} catch(PDOException $e){
			exit('PDO error ' . $e->getMessage());
		}

		if ($st->rowCount()){
			$row = $st->fetch();
			return new Korisnik($row['id_korisnika'], $row['nick'], $row['pass']);
		} else {
			return FALSE;
		}

	}

	public function getKorisnikNickByID($id){
		try {
			$db = DB::getConnection();
			$st = $db->prepare("SELECT nick FROM korisnik WHERE id_korisnika = ?");
			$st->execute([$id]);
		} catch(PDOException $e){
			exit('PDO error ' . $e->getMessage());
		}

		if ($st->rowCount()){
			$row = $st->fetch();
			return $row['nick'];
		} else {
			return FALSE;
		}


	}

	public function getKorisnikIdByNick($nick){

		try {
			$db = DB::getConnection();
			$st = $db->prepare("SELECT id_korisnika FROM korisnik WHERE nick = ?");
			$st->execute([$nick]);
		} catch(PDOException $e){
			exit('PDO error ' . $e->getMessage());
		}

		if ($st->rowCount()){
			$row = $st->fetch();
			return $row['id_korisnika'];
		} else {
			return FALSE;
		}

	}

	public function setKorisnik($nick, $pass, $ime, $prezime, $dat_rođenja, $mail, $gender, $interested_in, $about){

		try {
			$db = DB::getConnection();
			$st = $db->prepare("INSERT INTO korisnik (nick, pass, ime, prezime, dat_rođenja, gender, mail, interested_in, about)
				VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
			$st->execute([$nick, $pass, $ime, $prezime, $dat_rođenja, $gender, $mail, $interested_in, $about ]);
		} catch(PDOException $e){
			exit('PDO error ' . $e->getMessage());
		}

		return TRUE;

	}

	public function setKorisnik_interesi($temp_id, $korisnikov_interes, $poželjan_interes){
		try {
			$db = DB::getConnection();
			$st = $db->prepare("INSERT INTO korisnik_interes (id_korisnika, id_interesa, vrsta_relacije) VALUES (:id_k, :id_int, :tip)");
			foreach ($korisnikov_interes as $key) {
				$st->execute([
				'id_k' => $temp_id,
				'id_int' => $key,
				'tip' => 1
				]);
			}
			foreach ($poželjan_interes as $key) {
				$st->execute([
				'id_k' => $temp_id,
				'id_int' => $key,
				'tip' => 2
				]);
			}
		} catch(PDOException $e){
			exit('PDO error ' . $e->getMessage());
		}

		return TRUE;
	}

}
