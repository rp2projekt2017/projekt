<!DOCTYPE html>
<html>
	<head>
		<?php require_once __SITE_PATH . '/view/_head.php'; ?>
	</head>
	<body>
		<div class="container">

			<?php require_once __SITE_PATH . '/view/_naslov.php'; ?>

			<?php if (count($korisnici_opis)) { ?>
				<p class="boldBB"> Pogledajte neke od naših već registriranih korisnika: </p>
				<?php } else { ?>
				<p class="boldBB"> Trenutno nemamo registriranih korisnika, budite prvi! </p> <?php } ?>

			<?php foreach ($korisnici_opis as $clan):  ?>

				<?php $god = intval(date("Y"))-intval(substr($clan['dat_rođenja'], 0,4));; ?>
				<a href="<?= __SITE_URL ?>/korisnik/profil?nick=<?= $clan['nick'] ?>">
					<div class="naslovna">
						<div>
							<?php echo '<img src="./'.$clan['path_slike'].'" height="80" width="80">'; ?>
						</div>
						<p class="name"><?php echo $clan['ime'].', '.$god; ?></p>
						<p><?php echo $clan['about']; ?></p>
					</div>
 				</a>


			<?php endforeach; ?>


			<?php if (isset($_SESSION['korisnik'])): ?>

				<?php if (isset($poruka)): ?>
					<div class="row">
						<div class="col-xs-8 col-xs-offset-2">
							<div class="alert alert-danger" role="alert"><?= $porukaGreske ?></div>
						</div>
					</div>
				<?php endif; ?>

			<?php endif; ?>

		</div>
	</body>
</html>
