<!DOCTYPE html>
<html>
	<head>
		<?php require_once __SITE_PATH . '/view/_head.php'; ?>
	</head>
	<body>
		<div class="container">
			<?php require_once __SITE_PATH . '/view/_naslov.php'; ?>

			<div class="row">
				<div class="col-xs-12">
					<a href="<?= __SITE_URL ?>/"><span class="glyphicon glyphicon-home"></span> Povratak</a>
				</div>
			</div>

			<div class="chat">
				<?php foreach ($poruke as $poruka): ?>
				
					<?php if($_SESSION['korisnik']['nick'] == $poruka->sender): ?>

					<div>
					<article class=" Lijevo ">
						<h4><small><?= $poruka->vrijeme_slanja ?></small></h4>
						<h5>
							<div class="wrap">
							<div class="line sender"><strong><?= $poruka->sender ?></strong></div>
							<div class="line tekst"><?= $poruka->sadržaj ?></div>
							</div>
						</h5>
					</article>

					</div>

				<?php else: ?>

					<div>
					<article class=" Desno">
						<h4><small><?= $poruka->vrijeme_slanja ?></small></h4>
						<h5><div class="wrap">
							<div class="line tekst"><?= $poruka->sadržaj ?></div>
							<div class="line sender"><strong><?= $poruka->sender ?></strong></div>
							</div>
						</h5>
					</article>

					</div>
				<?php endif; ?>
				<?php endforeach; ?>
			</div>

			<?php if (isset($_SESSION['korisnik'])): ?>

				<?php if (isset($porukainfo)): ?>
					<div class="row">
						<div class="col-xs-8 col-xs-offset-2">
							<div class="alert alert-danger" role="alert"><?= $porukainfo ?></div>
						</div>
					</div>
				<?php endif; ?>

	

				<div class="row">
					<div class="col-xs-8 col-xs-offset-2">
						
						<form action="" method="POST">

							<input type="hidden" name="id" value="<?= $_GET['id'] ?>">

							<div class="form-group">
								<label for="sadržaj">Nova poruka</label>
								<textarea name="sadržaj" class="form-control" required maxlength="1000"><?= isset($_POST['sadržaj']) ? $_POST['sadržaj'] : "" ?></textarea>
							</div>

							<div class="form-group">
								<button class="btn btn-primary" name="akcija" value="post">Pošalji</button>
							</div>

						</form>

					</div>
				</div>

			<?php endif; ?>

		</div>
	</body>
</html>