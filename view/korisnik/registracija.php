<!DOCTYPE html>
<html>
	<head>
		<?php require_once __SITE_PATH . '/view/_head.php'; ?>
	</head>
	<body>
		<div class="container">
			<?php require_once __SITE_PATH . '/view/_naslov.php'; ?>

			<div class="row">
				<div class="col-xs-12">
					<a href="<?= __SITE_URL ?>/"><span class="glyphicon glyphicon-home"></span> Povratak</a>
				</div>
			</div>

			<?php if (isset($porukaGreske)): ?>
				<div class="row">
					<div class="col-xs-6 col-xs-offset-3">
						<div class="alert alert-danger" role="alert"><?= $porukaGreske ?></div>
					</div>
				</div>
			<?php endif; ?>

			<?php if (!isset($registracijaUspjela)): ?>

				<div class="pozadina">

					<div class="reg" style="padding-left: 3px;" id="polja">
						<form action="" method="POST">
							<label for="nick">Nadimak</label>
							<input id="nick" name="nick" type="text" required value="<?= isset($_POST['nick']) ?  $_POST['nick'] : ""?>"><br>
							<label for="pass">Lozinka</label>
							<input id="pass" name="pass" type="password" onkeyup="CheckPasswordStrength(this.value)" required value="<?= isset($_POST['pass']) ?  $_POST['pass'] : ""?>">
							<span id="password_strength"></span><br>
							<label for="ime">Ime</label>
							<input id="ime" name="ime" type="text" required value="<?= isset($_POST['ime']) ?  $_POST['ime'] : ""?>"><br>
							<label for="prezime">Prezime</label>
							<input id="prezime" name="prezime" type="text" required value="<?= isset($_POST['prezime']) ?  $_POST['prezime'] : ""?>"><br>
							<label for="bday">Datum rođenja</label>
							<input type="date" name="bday" required  value="<?= isset($_POST['bday']) ?  $_POST['bday'] : ""?>"><br>
							<label for="gender" required >Spol</label><br>
							<input type="radio" name="gender" value="m">Male<br>
							<input type="radio" name="gender" value="f">Female<br>
							<input type="radio" name="gender" value="o" checked>Other<br>
							<label for="email">E-mail</label>
							<input type="email" name="email" required value="<?= isset($_POST['email']) ?  $_POST['email'] : ""?>"><br>
							<label for="interested_in_gender" required>Zanima Vas</label><br>
							<input type="radio" name="interested_in_gender" value="m">Male<br>
							<input type="radio" name="interested_in_gender" value="f">Female<br>
							<input type="radio" name="interested_in_gender" value="o" checked>Both<br>
							<label for="about">Opis</label><br>
							<textarea name="about" cols="40" rows="5"><?= isset($_POST['about']) ?  $_POST['about'] : ""?></textarea><br>

					</div>

					<div class="reg" >
						<h4>Opišite se:</h4>
						<p>Što Vas zanima?</p>
							<?php foreach ($interesi as $interes): ?>
								<article>
									<input type="checkbox" name="korisnikov_interes[]" value="<?=$interes->id_interesa?>"> <?=$interes->ime_interesa?><br>

								</article>

							<?php endforeach; ?>
					</div>

					<div  class="reg">
						<h4>Opišite svoju bolju polovicu:</h4>
						<p>Koje su Vama poželjne osobine?</p>
							<?php foreach ($interesi as $interes): ?>
								<article>
									<input type="checkbox" name="poželjan_interes[]" value="<?=$interes->id_interesa?>"> <?=$interes->ime_interesa?><br>

								</article>

							<?php endforeach; ?>
					</div>

					<div id="send">
							<br><br><br>
							<button>Spremi</button>
						</div>
					</form>
				</div>

			<?php else: ?>


				<h3>Zahvaljujemo na registraciji!</h3>
				<p>Sada se možete prijaviti s vašim novim računom <strong><a href="<?= __SITE_URL ?>/korisnik/login">ovdje</a></strong>!</p>


			<?php endif; ?>
			<script type="text/javascript">
				function CheckPasswordStrength(password) {
					var password_strength = document.getElementById("password_strength");


					if (password.length == 0) {
	 				password_strength.innerHTML = "";
	 			return;
			}

			var regex = new Array();
			regex.push("[A-Z]"); 
			regex.push("[a-z]");
			regex.push("[0-9]");
			regex.push("[$@$!%*#?&]");

			var passed = 0;

			for (var i = 0; i < regex.length; i++) {
	 			if (new RegExp(regex[i]).test(password)) {
			 	passed++;
	 	}
	}

	if (passed > 2 && password.length > 8) {
	 	passed++;
	}

	var color = "";
	var strength = "";
	switch (passed) {
	 	case 0:
	 	case 1:
			 strength = "Slabo";
			 color = "#cc0000";
			 break;
	 	case 2:
			 strength = "Dobro";
			 color = "#803300";
			 break;
	 	 case 3:
		 		strength = "Vrlo dobro";
				color = "#FAD054";
	 	case 4:
			 strength = "Jako";
			 color = "green";
			 break;
	 	 case 5:
			 strength = "Vrlo jako";
			 color = "darkgreen";
			 break;
		 }
		 password_strength.innerHTML = strength;
		 password_strength.style.color = color;
	 }
	 </script>

		</div>
	</body>
</html>
