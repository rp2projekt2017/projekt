<!DOCTYPE html>
<html>
	<head>
		<?php require_once __SITE_PATH . '/view/_head.php'; ?>
	</head>
	<body>
		<div class="container">
			<?php require_once __SITE_PATH . '/view/_naslov.php'; ?>

<!--STRANICA JE ODVOJENA SA PUNO IF-OVA KOJI ODLUCUJU NA ČIJEM SMO TO PROFILU (OSOBNOM ILI NEČIJEM) TE TAKO ODLUCUJU PRIKAZ PROFILNE STRANICE-->

			<?php if($_GET['nick'] === "test" && $_SESSION['korisnik']['osoba'] === "test"): ?>

				<div class="row">
					<div class="col-xs-6 col-xs-offset-3">
						<div class="alert alert-danger" role="alert"><?= 'Nazalost u bazi trenutno nemamo osoba koje zadovoljavaju vaše kriterije :(
							<br /> Pokušajte ponovo kroz nekoliko dana kada će sigurno naša baza biti bogatija novim korisnicima!'?>		
						</div>
					</div>
				</div>

			<?php else: ?>

				<div class="row">
					<div class="col-xs-12">
						<a href="<?= __SITE_URL ?>/"><span class="glyphicon glyphicon-home"></span>  Povratak</a>
					</div>
				</div>

				<?php if (isset($poruka)): ?>
					<div class="row">
						<div class="col-xs-6 col-xs-offset-3">
							<div class="alert alert-danger" role="alert"><?= $poruka?></div>
						</div>
					</div>
				<?php endif; ?>
			
			
			<div class="podaci">

				<div class="text">
					<div><b>Ime :</b> <?php echo $podaci['ime']; ?></div>
					<div><b>Prezime : </b><?php echo $podaci['prezime']; ?></div>
					<div><b>Datum rođenja : </b><?php echo $podaci['dat_rođenja']; ?></div>
					<div><b>Spol : </b><?php if ($podaci['gender'] == 'm') echo "muški";
											elseif ($podaci['gender'] == 'f') echo "ženski";
											else echo "ostalo";?></div>
					<div><b>Zanimaju me : </b><?php if ($podaci['interested_in'] == 'm') echo "muškarci";
											elseif ($podaci['interested_in'] == 'f') echo "žene";
											else echo "oboje"; ?></div>

					<?php if ($_SESSION['korisnik']['id'] == $podaci['id_korisnika']): ?>
						<div><b>Mail : </b><?php echo $podaci['mail']; ?></div>
						<div><b> Moje osobine :</b>
							<?php foreach($interesi['imam'] as $val) echo ' '.$val.','; ?>
						</div>
						<div><b> Osobine koje tražim :</b>
							<?php foreach($interesi['trazim'] as $val) echo ' '.$val.','; ?>
						</div>
					<?php endif; ?>
					<div><b>O meni :</b> <?php echo $podaci['about']; ?></div>


					<?php if ($_SESSION['korisnik']['id'] !== $podaci['id_korisnika']): ?>
						<div>

							<?php if($udaljenost != -1): ?>
							<br><b><div>Udaljenost između vas i korisnika <?=$podaci['ime']?> iznosi <?=round($udaljenost, 2)?> kilometara.</div></b>


						<?php else: ?>
							<br><b><div>Udaljenost između vas i korisnika nazalost nije moguce odrediti</div></b>
						<?php endif; ?>

							<div class="podudaranje">Postotak podudaranja u interesima:</div>
							<div id="canvas">
								<canvas height="50" width="300" id="cnv" > </canvas>
							</div>

							<script>

							var a = <?=$posto;?>;
							var c = <?=round($posto*100,2);?>;
							var b = ((c).toString()).concat("%");
							var canvas = $("#cnv");
							var ctx = canvas[0].getContext("2d");
							ctx.fillStyle="#ff3333";
							ctx.fillRect(0, 0, canvas.width()*a, canvas.height());
							var img = new Image();
							img.src="./../upload_photos/heart.png"

							img.onload = function () {
							  	var pattern = ctx.createPattern(img, "repeat");
							    ctx.fillStyle = pattern;
							    ctx.fillRect(0, 0,canvas.width()*a, canvas.height());
							    ctx.textAlign="center";
								ctx.textBaseline="middle";
								ctx.font="bold 35px Arial";
								ctx.fillStyle="#404040";
								ctx.fillText(b, canvas.width()/2, canvas.height()/2);
							}

							</script>
						</div>
					<?php endif; ?>
					
					<?php $sena = './..'.$podaci['path_slike'];  ?>
				</div>


			</div>


			<div class="desno">
				<div style="text-align: center;"><img class="profilna" src=<?= $sena;?> > </div>

				<?php if ($_SESSION['korisnik']['id'] == $podaci['id_korisnika']): ?>
					<div style="text-align: center" >
					<?php if("/upload_photos/Not_available.jpg" === $podaci['path_slike']) :?>
						<form  method="post" enctype="multipart/form-data" action="<?= __SITE_URL ?>/korisnik/upload">
							<input  type="file" name="document" />
							<button type="submit"><span class="glyphicon glyphicon-camera"> </span> POSTAVI SLIKU PROFILA </button>
						</form>
					<?php else:?>
						<form  method="post" enctype="multipart/form-data" action="<?= __SITE_URL ?>/korisnik/upload">
							<input  type="file" name="document" />
							<button type="submit">PROMJENI SLIKU PROFILA</button>
						</form>
					<?php endif; ?>
					</div>

				<?php else:?>
					<div style="text-align: center; margin-top: 10px;">
					<a class="btn btn-default" href="<?= __SITE_URL ?>/razgovor?id=<?=$podaci['id_korisnika']?>"><span class="glyphicon glyphicon-envelope">  </span>   RAZGOVOR</a>
					</div>
				<?php endif; ?>
			
			</div>
			<div class="buttons">
				<?php if ($_SESSION['korisnik']['id'] == $podaci['id_korisnika']): ?>
					
						<a class="btn btn-default" href="<?= __SITE_URL ?>/korisnik/profil?nick=<?php echo $_SESSION['korisnik']['osoba'];?>"><span class="glyphicon glyphicon-heart">  </span>   TVOJA SRODNA DUSA!  <span class="glyphicon glyphicon-heart">  </span></a>

						<a class="btn btn-default" href="<?= __SITE_URL ?>/popis"><span class="glyphicon glyphicon-envelope">  </span>  MOJE PORUKE</a>

				<?php else: ?>
					
						<form method="post"enctype="multipart/form-data" action="<?= __SITE_URL ?>/korisnik/posalji">
							<input type="hidden" name="primatelj" value=<?= $podaci['id_korisnika']; ?> >
							<textarea name="poruka" rows="5" cols="40"></textarea><br>
							<input class="glyphicon glyphicon button"  type="submit" value = "POŠALJI PORUKU OSOBI "/>
						</form>
					
				<?php endif; ?>

			</div>
			<?php endif; ?>
		</div>

		
	</body>
</html>
