<!DOCTYPE html>
<html>
	<head>
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

		<?php require_once __SITE_PATH . '/view/_head.php'; ?>
	</head>
	<body>
		<div class="container">
			<?php require_once __SITE_PATH . '/view/_naslov.php'; ?>

			<div class="row">
				<div class="col-xs-12">
					<a href="<?= __SITE_URL ?>/"><span class="glyphicon glyphicon-home"></span> Povratak</a>
				</div>
			</div>

			<?php if (isset($porukaGreske)): ?>
				<div class="row">
					<div class="col-xs-6 col-xs-offset-3">
						<div class="alert alert-danger" role="alert"><?= $porukaGreske ?></div>
					</div>
				</div>
			<?php endif; ?>

			<?php if (!isset($_SESSION['korisnik'])): ?>

			<div class="row">
				<div class="col-xs-6 col-xs-offset-3">
					<div class="panel panel-default">
						<div class="panel-body">
							<form class="form-horizontal" action="" method="POST">
								<div class="form-group">
									<label class="col-md-4 control-label" for="nick">Nadimak</label>
									<div class="col-md-6">
										<input id="nick" name="nick" type="text" class="form-control input-md" required>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-4 control-label" for="pass">Lozinka</label>
									<div class="col-md-6">
										<input id="pass" name="pass" type="password" class="form-control input-md" required>
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-4 col-md-offset-4">
										<button class="btn btn-primary"><span class="glyphicon glyphicon-heart"> </span> Prijava</button>
									</div>
									<input type="text" name="lat" id="lat" value="" hidden>
									<input type="text" id="lng" name="lng" value="" hidden>
								</div>
							</form>
						</div>
						<div class="panel-footer">
							Ako nemate korisnički račun možete ga napraviti <strong><a href="<?= __SITE_URL ?>/korisnik/registracija">ovdje</a></strong>.
						</div>
					</div>
				</div>
			</div>

			<?php else: ?>

			<div class="row">
				<div class="col-xs-6 col-xs-offset-3">
					<div class="panel panel-default">
						<div class="panel-body">
							<h3>Prijavljeni ste kao <?= $_SESSION['korisnik']["nick"] ?></h3>
							<p>Svoj profil pregledajte <strong><a href="<?= __SITE_URL ?>/korisnik/profil?nick=<?= $_SESSION['korisnik']["nick"]?>">ovdje</a></strong>!</p>
						</div>
					</div>
				</div>
			</div>	

			<?php endif; ?>

		</div>
	</body>
</html>