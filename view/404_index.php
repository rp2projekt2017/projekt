<!DOCTYPE html>
<html>
	<head>
		<?php require_once __SITE_PATH . '/view/_head.php'; ?>
	</head>
	<body>
		<div class="container">
			<?php require_once __SITE_PATH . '/view/_naslov.php'; ?>

			<div class="row">
				<div class="col-xs-12">
					<a href="<?= __SITE_URL ?>/"><span class="glyphicon glyphicon-arrow-left"></span> Povratak</a>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12">
					<h2 class="text-center"><strong>Greška 404</strong> - Stranica koju ste probali otvoriti ne postoji!</h2>
				</div>
			</div>

		</div>
	</body>
</html>