<div class="row">
	<div class="page-header">
		<div class="col-xs-10">
			<div class="logo">
				<img src=<?=__SITE_URL.'/upload_photos/logo.jpg' ?> >
			</div>
				<h1>Stranica za upoznavanje <br><small><?= isset($naslov) ? $naslov : "Dobrodošli!"?></small></h1>
		</div>
		<div class="col-xs-2">
			<div class="login-widget">
				<?php if (isset($_SESSION['korisnik'])) : ?>

					Prijavljeni ste kao: <strong><?= $_SESSION['korisnik']['nick'] ?></strong>
					<div>
						<strong><a  href="<?= __SITE_URL ?>/korisnik/profil?nick=<?= $_SESSION['korisnik']["nick"]?>"><span class="glyphicon glyphicon-user"></span>
						Moj profil!  </a></strong>
						<strong><a  href="<?= __SITE_URL ?>/popis?"><span class="glyphicon glyphicon-envelope"></span>
						Moje poruke!</a></strong>
					</div>
					<?php if($_SESSION['korisnik']['poruka']): ?>
						<p id="poruka_p">Imate novu poruku!</p>
					<?php endif; ?>
					<a class="btn btn-default" href="<?= __SITE_URL ?>/korisnik/logout"><span class="glyphicon glyphicon-heart-empty"></span> Odjava</a>

				<?php else: ?>
					Niste prijavljeni.
					<a class="btn btn-default" href="<?= __SITE_URL ?>/korisnik/login"><span class="glyphicon glyphicon-heart"></span> Prijava</a>
				<?php endif; ?>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
