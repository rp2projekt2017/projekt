<!DOCTYPE html>
<html>
	<head>
		<?php require_once __SITE_PATH . '/view/_head.php'; ?>
	</head>
	<body>
		<div class="container">
			<?php require_once __SITE_PATH . '/view/_naslov.php'; ?>

			<div class="row">
				<div class="col-xs-12">
					<a href="<?= __SITE_URL ?>/"><span class="glyphicon glyphicon-home"></span> Povratak</a>
				</div>
			</div>
			<div class="row">
				<?php foreach ($razgovori as $razgovor): ?>
					
					<article class="col-xs-12"> 
						<h4>
							<a href="<?= __SITE_URL ?>/korisnik/profil?nick=<?= $razgovor['kontakt'] ?>"><?= $razgovor['kontakt'] ?> </a>
						</h4>
						<h5>
							<a href="<?= __SITE_URL ?>/razgovor?id=<?= $razgovor['id'] ?>"><?= "broj razmijenjenih poruka: " . $razgovor['count']?> </a>
						</h5>
						
						<h5>
							<?= "vrijeme zadnje poruke: " . $razgovor['last']?>
						</h5>

						 
					</article>
				
				<?php endforeach; ?>
			
	</body>
</html>