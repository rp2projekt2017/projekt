<?php 

class RazgovorController extends BaseController{

	public function index(){
		$ss = new SiteService();

		$temp_nick2 = $ss->getKorisnikNickByID($_GET['id']);
		if($temp_nick2 === FALSE || ($_SESSION['korisnik']['id'] == $_GET['id'])){
			header('Location: ' . __SITE_URL . '/404');
		} else {
			$this->registry->template->naslov = "Vaš razgovor s osobom: " . $temp_nick2;
		

		if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_SESSION['korisnik'])){

			$ss->ubijPoruku($_SESSION['korisnik']['id']);
			$poruka = $ss->provjeriPoruku($_SESSION['korisnik']['id']);
			$_SESSION['korisnik']['poruka'] = $poruka;

			
			if(!strlen($_POST['sadržaj'])){
				$this->registry->template->porukainfo = "Niste upisali poruku!";
			} else {
				$ss->posaljiPoruku($_SESSION['korisnik']['id'], $_POST['id'], nl2br(trim($_POST['sadržaj'])));				
				$this->registry->template->porukainfo = "Poruka uspješno poslana!";
				unset($_POST);
			}
			

		}  
			$poruke = $ss->getPorukeByReceiverAndSenderID($_SESSION['korisnik']['id'],$_GET['id']);
			foreach ($poruke as $poruka) {
				$poruka->sender = $ss->getKorisnikNickByID($poruka->id_sender); 
			}
			$this->registry->template->poruke = $poruke;

			$this->registry->template->show('razgovor_index');
		}

		
	}

}; 