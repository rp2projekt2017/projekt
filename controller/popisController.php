<?php 

class PopisController extends BaseController{

	public function index(){
		$ss = new SiteService();

		if(isset($_SESSION['korisnik'])){
			$ss->ubijPoruku($_SESSION['korisnik']['id']);
			$poruka = $ss->provjeriPoruku($_SESSION['korisnik']['id']);
			$_SESSION['korisnik']['poruka'] = $poruka;

			$sve = $ss->getPopisById($_SESSION['korisnik']['id']);
			$this->registry->template->razgovori = $sve;
			$this->registry->template->naslov = "Popis Vaših razgovora: ";
			$this->registry->template->show('popis');

		} else{
			header('Location: ' . __SITE_URL . '/korisnik/login');
			$this->registry->template->show('korisnik/login');
		}

	}

};
