<?php //(T)staro, stoji samo da ostalo radi

class IndexController extends BaseController
{



	public function index(){
		$ss = new SiteService();

		if(isset($_SESSION['korisnik'])){
			$poruka = $ss->provjeriPoruku($_SESSION['korisnik']['id']);
			$_SESSION['korisnik']['poruka'] = $poruka;
		}

		$interesi = $ss->getSviInteresi();


		$korisnici_opis = $ss->getImeOpisDatum();
		$this->registry->template->korisnici_opis = $korisnici_opis;

		// Samo preusmjeri na podstranicu.
		$this->registry->template->show('index_index');
	}

};
