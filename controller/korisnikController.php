<?php

class KorisnikController extends BaseController{

	public function index(){
		header('Location: ' . __SITE_URL . '/korisnik/login');
		
	}

	public function login(){

		$this->registry->template->naslov = "Prijava korisnika";

		if ($_SERVER['REQUEST_METHOD'] === 'POST'){

			if(!strlen($_POST['nick']) || !strlen($_POST['pass'])){
				$this->registry->template->porukaGreske = "Morate popuniti sva polja!";
			} else {

				$ss = new SiteService();

				$korisnik = $ss->getKorisnikByNick($_POST['nick']);

				if($korisnik === FALSE && !isset($_SESSION['korisnik']) || $korisnik === FALSE && $_SESSION['korisnik']['osoba'] !== "test"){
						$this->registry->template->porukaGreske = "Korisnik ". $_POST['nick'] ." nije pronađen.";
				}elseif (!password_verify($_POST['pass'], $korisnik->pass)){
					$this->registry->template->porukaGreske = "Lozinka neispravna!";
				} else {

					$ss->setLocation($korisnik->id);
					$osoba = $ss->nadiNajboljeg($korisnik->id);
					if ($osoba !== -1) $moja = $ss->getKorisnikNickByID($osoba);
					else $moja="test";
					$poruka = $ss->provjeriPoruku($korisnik->id);
 					$_SESSION['korisnik'] = [
 						'id' => $korisnik->id,
 						'nick' => $korisnik->nick,
						'id_osobe' => $osoba,
						'osoba' => $moja,
						'poruka' => $poruka
  					];
 				}



			}

		}

        $this->registry->template->show('korisnik/login');
	}

	public function logout(){

		session_unset();
		session_destroy();
		$this->registry->template->naslov = "Žao nam je što nas napuštate :(";

		header('Location: ' . __SITE_URL . '/');
	}


	public function profil(){

		$ss = new SiteService();
		

		if(isset($_SESSION['korisnik'])){
			$provjeraNicka = $ss->getKorisnikByNick($_GET['nick']);
			if($provjeraNicka === FALSE){
				header('Location: ' . __SITE_URL . '/404');

			} else {
				$poruka = $ss->provjeriPoruku($_SESSION['korisnik']['id']);
				$_SESSION['korisnik']['poruka'] = $poruka;

				$temp_nick = $_GET['nick'];
				if($temp_nick == $_SESSION['korisnik']['nick']){
					$this->registry->template->naslov = "Vaš profil: ";
				}else{

					$this->registry->template->naslov = "Profil korisnika: " . $temp_nick;
				}

				$podaci = $ss->getPodacibyNick($temp_nick);
				$temp_id = $ss->getKorisnikIdByNick($temp_nick);
				$temp_nick = $ss->getKorisnikNickByID($temp_id);
				$imam = $ss->getInteresibyIDkorisnika($temp_id, 1);
				$trazim = $ss->getInteresibyIDkorisnika($temp_id, 2);

				$interesi = [
					'imam' => $imam,
					'trazim' => $trazim
				];

				$posto = $ss->postoPodudaranja($_SESSION['korisnik']['id'], $temp_id);
				$udaljenost = $ss->Udaljenost($_SESSION['korisnik']['nick'], $temp_nick);

				$this->registry->template->udaljenost = $udaljenost;
				$this->registry->template->posto = $posto;
				$this->registry->template->podaci = $podaci;
				$this->registry->template->interesi = $interesi;
				$this->registry->template->show('korisnik/profil_index');
			}
		} else{
			header('Location: ' . __SITE_URL . '/korisnik/login');
			$this->registry->template->show('korisnik/login');
		}

	}

	public function posalji(){
		$ss = new SiteService();
		$pov = $ss->posaljiPoruku($_SESSION['korisnik']['id'], $_POST['primatelj'], $_POST['poruka']);
		$this->registry->template->naslov = "Vaš profil: ";

		$temp_id= $_POST['primatelj'];
		$temp_nick= $ss->getKorisnikNickByID($temp_id);
		$podaci = $ss->getPodacibyNick($temp_nick);
		$imam = $ss->getInteresibyIDkorisnika($temp_id, 1);
		$trazim = $ss->getInteresibyIDkorisnika($temp_id, 2);

		$interesi = [
			'imam' => $imam,
			'trazim' => $trazim
		];

		$posto = $ss->postoPodudaranja($_SESSION['korisnik']['id'], $temp_id);
		$this->registry->template->posto = $posto;
		$udaljenost = $ss->Udaljenost($_SESSION['korisnik']['nick'], $temp_nick);
		$this->registry->template->udaljenost = $udaljenost;
		$_GET['nick'] = $temp_nick;
		$this->registry->template->poruka="Poruka uspjesno poslana";
		$this->registry->template->podaci = $podaci;
		$this->registry->template->interesi = $interesi;
		$this->registry->template->show('korisnik/profil_index');
	}

	public function upload(){
		$ss = new SiteService();
		$this->registry->template->naslov = "Vaš profil: ";

		if(isset( $_FILES['document']) && ($_FILES['document']['error'] == UPLOAD_ERR_OK))
		{
			$path = './upload_photos/' .$_FILES['document']['name'];
			$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
			$ext = pathinfo($_FILES["document"]["name"], PATHINFO_EXTENSION);
			if($_FILES['document']['size'] > 512000)
				 $this->registry->template->poruka ="NIje dozvoljeno slati datoteke vece od 500 kB";
			else if (!array_key_exists($ext, $allowed))
				$this->registry->template->poruka="Samo slike jpg, gif,png, jpeg formata je dozvoljeno";
			else if (move_uploaded_file( $_FILES['document']['tmp_name'], $path)){
				$sad = dirname(__DIR__) . $ss->getImgPath($_SESSION['korisnik']['id']);
				if ($ss->getImgPath($_SESSION['korisnik']['id']) !== "/upload_photos/Not_available.jpg") unlink($sad);
				$path = substr($path, 1);
				if ($ss->setImgPath($path ,$_SESSION['korisnik']['id']))
					$this->registry->template->poruka="Datoteka ".$_FILES['document']['name']." je stavljena na profil";
			} else
				$this->registry->template->poruka="Ne mogu spremiti datoteku ".$_FILES['document']['name'];
 		}
		else {  $this->registry->template->poruka="Nije nijedan file poslan"; }

		$temp_nick= $_SESSION['korisnik']['nick'];
		$podaci = $ss->getPodacibyNick($temp_nick);
		$temp_id = $ss->getKorisnikIdByNick($temp_nick);
		//$osoba = $ss->nadiNajboljeg($korisnik->id);
		$imam = $ss->getInteresibyIDkorisnika($temp_id, 1);
		$trazim = $ss->getInteresibyIDkorisnika($temp_id, 2);

		$interesi = [
			'imam' => $imam,
			'trazim' => $trazim
		];

		$_GET['nick'] = $temp_nick;
		$this->registry->template->podaci = $podaci;
		$this->registry->template->interesi = $interesi;
		$this->registry->template->show('korisnik/profil_index');
	}

	public function registracija(){ // (T)ovo je skroz novo

		$ss = new SiteService();

		$this->registry->template->naslov = "Registracija korisnika";

		$interesi = $ss->getSviInteresi();

		$this->registry->template->interesi = $interesi;

		if ($_SERVER['REQUEST_METHOD'] === 'POST'){
			if(!strlen($_POST['nick']) || !strlen($_POST['pass']) || !strlen($_POST['ime']) || !strlen($_POST['prezime'])
			 || !strlen($_POST['bday']) || !strlen($_POST['email'] || !isset($_POST['gender']) || !isset($_POST['interested_in_gender']))){
				$this->registry->template->porukaGreske = "Morate popuniti sva polja!";
			}
			elseif(isset($_POST['korisnikov_interes']) && isset($_POST['poželjan_interes'])){
    			 /*check atleast 2 checked*/
     			if(sizeof($_POST['korisnikov_interes']) < 2 || sizeof($_POST['poželjan_interes']) < 2){
      				 $this->registry->template->porukaGreske = "Morate označiti barem dva boxa!";
   				} else{

					if(!preg_match( '/^[a-zA-Z0-9]{3,20}$/', $_POST['nick'])){ //tu treba dodati još provjera za ime, prezime, mail itd...
					$this->registry->template->porukaGreske = "Korisničko ime treba imati između 3 i 20 znakova. Dozvoljena su samo slova i brojke.";
					} else {

						$ss = new SiteService();

						$provjeraNicka = $ss->getKorisnikByNick($_POST['nick']);

						if ($provjeraNicka === FALSE){
							$ss->setKorisnik($_POST['nick'], password_hash($_POST['pass'], PASSWORD_DEFAULT), $_POST['ime'], $_POST['prezime'],
								$_POST['bday'], $_POST['email'], $_POST['gender'], $_POST['interested_in_gender'], $_POST['about']);
							$temp_id = $ss->getKorisnikIdByNick($_POST['nick']);
							$ss->setKorisnik_interesi($temp_id, $_POST['korisnikov_interes'], $_POST['poželjan_interes']);
							$this->registry->template->registracijaUspjela = TRUE;
						} else {
							$this->registry->template->porukaGreske = "Korisnik s ovim nadimkom već postoji!";
						}
					}
				}
			} else{
				$this->registry->template->porukaGreske = "Morate označiti interese!";
			}
		}

		$this->registry->template->show('korisnik/registracija');

	}
};
