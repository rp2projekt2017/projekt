<?php //(T) izbačen nick_hash, dodan path za opću sliku, nije testiran

// Inicijalizacija baze
require_once '../../model/db.class.php';

$db = DB::getConnection();

try {
	$st = $db->prepare('
		CREATE TABLE interes (
			id_interesa int UNSIGNED NOT NULL AUTO_INCREMENT,
			ime_interesa varchar(25) NOT NULL,
			PRIMARY KEY (id_interesa),
			UNIQUE KEY key_ime_interesa (ime_interesa)
		)
	');

	$st->execute();
} catch(PDOException $e) {
	exit("PDO error #1: " . $e->getMessage());
}

echo "Napravila tablicu interes.<br>";


try {
	$st = $db->prepare('
		CREATE TABLE korisnik (
			id_korisnika int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
			nick varchar(25) NOT NULL,
			pass varchar(255) NOT NULL,
			ime varchar(25) NOT NULL,
			prezime varchar(25) NOT NULL,
			dat_rođenja date NOT NULL,
			gender varchar(1) NOT NULL DEFAULT "O",
			path_slike varchar(50) DEFAULT "/upload_photos/Not_available.jpg",
			lat float DEFAULT NULL,
			lng float DEFAULT NULL,
			mail varchar(50) DEFAULT NULL,
			interested_in varchar(1) NOT NULL DEFAULT "x",
			about varchar(300) DEFAULT NULL,
			poruka_flag int(1) DEFAULT 0,
			PRIMARY KEY (id_korisnika)
		)
	');

	$st->execute();
} catch(PDOException $e) {
	exit("PDO error #2: " . $e->getMessage());
}

echo "Napravila tablicu korisnik.<br>";

try {
	$st = $db->prepare('INSERT INTO korisnik (nick, pass, ime, prezime, dat_rođenja) VALUES (:nick, :pass, :ime, :prezime, :dat)');
	$st->execute(['nick' => 'test', 'pass' => 'test', 'ime' => 'test', 'prezime' => 'test', 'dat' => '1994-09-12']);
	} catch(PDOException $e) {
		exit("PDO error #3: " . $e->getMessage());
	}

echo "Ubačena nužna osoba za rad stranice.<br>";

try {
	$st = $db->prepare('
		CREATE TABLE korisnik_interes (
			id_korisnika int UNSIGNED NOT NULL AUTO_INCREMENT,
			id_interesa int UNSIGNED NOT NULL,
			vrsta_relacije int(1) NOT NULL,
			PRIMARY KEY index_korisnik_interes (id_korisnika, id_interesa, vrsta_relacije)
		)
	');

	$st->execute();
} catch(PDOException $e) {
	exit("PDO error #4: " . $e->getMessage());
}

echo "Napravila tablicu korisnik_interes.<br>";

try {
	$st = $db->prepare('
		CREATE TABLE poruka (
			id_poruke int UNSIGNED NOT NULL AUTO_INCREMENT,
			id_sender int UNSIGNED NOT NULL,
			id_receiver int NOT NULL,
			sadržaj varchar(300) DEFAULT NULL,
			vrijeme_slanja timestamp NULL DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY (id_poruke)
		)
	');

	$st->execute();
} catch(PDOException $e) {
	exit("PDO error #5: " . $e->getMessage());
}

echo "Napravila tablicu poruka.<br>";



// Ubaci interese
try {
	$st = $db->prepare('INSERT INTO interes (id_interesa, ime_interesa) VALUES (:id, :ime)');

	$st->execute([
		'id' => 1,
		'ime' => 'film'
	]);
	$st->execute([
		'id' => 2,
		'ime' => 'sport'
	]);
	$st->execute([
		'id' => 3,
		'ime' => 'glazba'
	]);
	$st->execute([
		'id' => 4,
		'ime' => 'izlasci'
	]);
	$st->execute([
		'id' => 5,
		'ime' => 'pušenje'
	]);
	$st->execute([
		'id' => 6,
		'ime' => 'knjige'
	])
	;$st->execute([
		'id' => 7,
		'ime' => 'putovanja'
	]);
	$st->execute([
		'id' => 8,
		'ime' => 'kazalište'
	]);
	$st->execute([
		'id' => 9,
		'ime' => 'karijera'
	]);
	$st->execute([
		'id' => 10,
		'ime' => 'djeca'
	])
	;$st->execute([
		'id' => 11,
		'ime' => 'ples'
	]);
	$st->execute([
		'id' => 12,
		'ime' => 'kuhanje'
	]);
	$st->execute([
		'id' => 13,
		'ime' => 'životinje'
	]);
	$st->execute([
		'id' => 14,
		'ime' => 'religija'
	])
	;$st->execute([
		'id' => 15,
		'ime' => 'politika'
	]);
	$st->execute([
		'id' => 16,
		'ime' => 'priroda'
	]);
	$st->execute([
		'id' => 17,
		'ime' => 'humor'
	]);

} catch(PDOException $e) {
	exit("PDO error #6: " . $e->getMessage());
}

echo "Ubacila teme u tablicu teme.<br>";
